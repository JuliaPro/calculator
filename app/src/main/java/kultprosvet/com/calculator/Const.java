package kultprosvet.com.calculator;

public class Const {
    protected static final int ZERO_VALUE = 0;
    protected static final int ONE_VALUE = 1;
    protected static final String EMPTY = "";
    protected static final String ONE = "1";
    protected static final String TWO = "2";
    protected static final String THREE = "3";
    protected static final String FOUR = "4";
    protected static final String FIVE = "5";
    protected static final String SIX = "6";
    protected static final String SEVEN = "7";
    protected static final String EIGHT = "8";
    protected static final String NINE = "9";
    protected static final String ZERO = "0";
    protected static final String COMMA = ".";
    protected static final String OPER_MULT = "*";
    protected static final String OPER_DELIM = "/";
    protected static final String OPER_PLUS = "+";
    protected static final String OPER_MINUS = "-";
    protected static final String EQUALS = "=";
    protected static final String CLEAR = "C";
    protected static final String TOGGLE = "+/-";
}
